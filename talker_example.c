/**
 * @file talker_example.c
 * @author Santiago Torres Borda (santiago.torres-borda@isae-supaero.fr)
 * @brief This is a brief example on how to use tx features of POSIX Sockets through a custom library named posix_sockets with scheduling capabilities
 * @version 1.0
 * @date 2022-10-21
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#define PORT 30000 // Destination port (This allows the receiver to understand what kind of message it has just been sent)
#define STACK_SZ 100 // Number of integers than can be stocked inside a specific class' stack
#define EXECUTION_TIME_MAX 10000 // number of clock steps this code will take to execute

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include "lib/posix_sockets.h"


/**
 * @brief fill a stack of pseudorandom numbers
 *
 * The generated stack will have a fixed number of STACK_SZ pseudorandom numbers
 * 
 * @param traffic_class a pointer to the first address of the stack to fill
 */
void fillStack(int *traffic_class){
    int lower_thresh = 0, upper_thresh = 32767;
    int index = 0;
    
    // Use current time as seed for random generator
    srand(time(0));
    for(index = 0; index<STACK_SZ; index++){
        *(traffic_class+index) = (rand()%(upper_thresh - lower_thresh + 1)) + lower_thresh;
    }
}

/**
 * @brief convert epoch to ns
 *  
 * Convert seconds gone by since 1/1/1970 formatted in s.ns to ns
 *
 * @param time timekeeping structure
 * @return converted nanoseconds
 */
long long int epoch_s2ns(struct timespec time){
    return (time.tv_sec*(1000000000)) + time.tv_nsec;
}

int socket_handle;
int error_handle;

int main (){
    /* Initialize variables for time keeping */
    unsigned long previous_ns = 0;  // stores the last time a datagram was sent

    // constants won't change
    const long interval = 100000;  // interval at which the message will be scheduled (nanoseconds)
    
    /* Data for time keeping */
    struct timespec time_now; // stores local time
    long long int current_ns = 0; // stores local time in nanoseconds

    /* Data to be sent */
    char data_tx[100]; // message (a.k.a. payload)

    /* Socket creation */
    socket_handle = init_socket(); 

    /* Variables for data generation */
    int classA[STACK_SZ];
    int classB[STACK_SZ];
    short index_ClassA = 0;
    short index_ClassB = -1;
    int clock_counter_classA = 0;
    int clock_counter_classB = 0;

    /* Fill both stacks with data (This isn't representative of real life implementations, but is a good aproximation and simplifies the problem)*/
    fillStack(classA);
    fillStack(classB);

    /* Variables that are used only for the example (won't be needed in the completed exercise)*/
    int execution_time = 0;
    int classA_sent_pckts = 0;
    int classB_sent_pckts = 0;
    /* Initialize scheduling algorithm */
    while(1){
        // Obtain system time
        clock_gettime(CLOCK_REALTIME, &time_now);

        current_ns = epoch_s2ns(time_now);

        //printf("current_ns: %lld\n", current_ns);

        if(current_ns - previous_ns >= interval){
            // initiate CQF algorithm
            // 1. Open gates for class A
            // 2. Start emitting for class A
            // 3. Once time reaches its end close gate A and open gate B
            // 4. Wait for class' B timer to end.
            // 5. Modulate gating time to prioritize a specific class
            // NOTE: do not forget to regen the data by calling fillStack()
            
            // EXAMPLE OF DATA TX
            // error_handle = send_data(socket_handle, data_tx, PORT); 
            
            if(index_ClassA >= 0 && index_ClassB == -1){
                sprintf(data_tx,"%d",(int)classA[index_ClassA]); // Convert integer to string
                printf("Data to be transmitted: %s \t| Class A\n", data_tx);
                classA_sent_pckts++;                
                index_ClassA++;
                if(index_ClassA == STACK_SZ){
                    index_ClassB = 0;
                    index_ClassA = -1;
                }
            }

            else if(index_ClassA == -1 && index_ClassB >= 0){
                sprintf(data_tx,"%d",classB[index_ClassB]); // Convert integer to string
                printf("Data to be transmitted: %s \t| Class B\n", data_tx);
                classB_sent_pckts++;
                index_ClassB++;
                if(index_ClassB == STACK_SZ){
                    index_ClassA = 0;
                    index_ClassB = -1;
                }
            }

            previous_ns = current_ns;

            execution_time++;
            if(execution_time >= EXECUTION_TIME_MAX){
                printf("\n[INFO] Simulated time: %ld ns\n", interval*EXECUTION_TIME_MAX);
                printf("[INFO] Tx timestep: %ld ns\n", interval);
                printf("[INFO] Packets sent from class A:%d \t | \t Packets sent from class B:%d\n",classA_sent_pckts,classB_sent_pckts);
                return EXIT_SUCCESS;   
            }         
        }
    }
}