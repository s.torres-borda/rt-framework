/**
 * @file listener_example.c
 * @author Santiago Torres Borda (santiago.torres-borda@isae-supaero.fr)
 * @brief This is a brief example on how to use rx features of POSIX Sockets through a custom library named posix_sockets
 * @version 1.0
 * @date 2022-10-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */
   
#define PORT 30000 // Destination port (This allows the receiver to understand what kind of message it has just been sent)

#include "lib/posix_sockets.h"

int socket_handle;

// Driver code 
int main(){ 
    char buffer[MAXLINE]; // String where received data will be contained
    int received_bytes_num; // Length of the received string

    /* Socket creation */
    socket_handle = init_socket(); 
    bind_socket(socket_handle, PORT); // Bind socket to a port
    
    /* Data reception */
    while(1){
        received_bytes_num = recv_data(socket_handle, buffer, PORT);

        buffer[received_bytes_num] = '\0'; // A null termination is added for correct string interpretation
        printf("Talker said: %s\n", buffer);
    }
}