##
 # @file Makefile
 # @author Santiago Torres Borda (santiago.torres-borda@isae-supaero.fr)
 # @brief This is the makefile that allows the user to compile posix_sockets library alongside the codes that derive from it.
 # @version 1.0
 # @date 2022-10-21
 # 
 # @copyright Copyright (c) 2022
##
CC      := gcc
CFLAGS  := -Wall -Wextra -Werror
LDFLAGS := -g
DEPS := lib/posix_sockets.c
TALKER_EXAMPLE := talker_example.c 
LISTENER_EXAMPLE := listener_example.c
OBTAIN_TIME_EXAMPLE := obtain_time_example.c

all: talker_example listener_example obtain_time_example

talker_example:
	mkdir -p bin/
	$(CC) $(LDFLAGS) -o bin/$@ $(TALKER_EXAMPLE) $(DEPS)

listener_example:
	mkdir -p bin/
	$(CC) $(LDFLAGS) -o bin/$@ $(LISTENER_EXAMPLE) $(DEPS)

obtain_time_example:
	mkdir -p bin/
	$(CC) $(LDFLAGS) -o bin/$@ $(OBTAIN_TIME_EXAMPLE)

clean:
	rm -rf bin/*
