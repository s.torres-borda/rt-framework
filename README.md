# Introduction
Real-Time Embedded systems can be found whenever a _critical_ system that involves computer systems is identified to be needed within a more complex system. The perfect example for such systems are airplanes. If certain functionalities of the airplane fail, then it might fall. These *critical embedded systems* are conceived in such a way that reliability and availability are the priority within the design. Going back to the airplane example, the airplane's autopilot relies on different sensors to manage its control loop. These sensors need to be exchanging their values at a specific rate in order for the control loop to function correctly. In practice, designing a system that would collect the information from the sensors and process it would be interconnected embedded systems within a star topology. In this topology, two types of embedded systems would be found, the sensing ones, which have the job to collect information and send it at a specified rate and a processing unit which would have the job to receive the sent information and treat it through the bias of the autopilot's control loop. Nevertheless, it isn't as simple, it suffices that one of the elements within the star fails for the system to fail. This would result in cathastrophic events, which isn't what would be expected from the end user. 

Real-Time embedded systems answer to the needs of transforming the possibly cathastrophic into something safe. There are two ways in which they do that. The first one is by creating redundancy within the system, so that if a link is lost, another can take its place. The second one is by ensuring data transfers are done whenever they need to be done. This repository focuses on the latter. As is explained in **doc/**, the solutions to these problems thrive. Nevertheless, none is as complete and as revolutionary as Time Sensitive Networking (TSN). This repository, in theory, allows a framework to build whatever mechanism is in the standard (provided you use Linux off course). 

## The exercise
Today's exercise will focus on building the Cyclic Queing and Forwarding algorithm described in standard IEEE 802.1Qch. The algorithm follows the block diagram bellow:

<img src="doc/CQF_bd.png">

The simplificated model of CQF considers two queues contained inside a talker device. The talker must first seggregate data between the queues according to what benefits priority data scheduling. Once data is seggregated within the two queues, it will proceed to allow the transmission of the data only from the first queue for a specific amount of time, and then, for the same amount of time, it will proceed to allow only the second queue to transmit. Then the process will repeat itself.

Your job is to implement this algorithm and play with it so it can ressemble what the Time Aware Shaper (IEEE 802.1Qbv) following what is explained within *talker_example.c*.

```c
// initiate CQF algorithm
// 1. Open gates for class A
// 2. Start emitting for class A
// 3. Once time reaches its end close gate A and open gate B
// 4. Wait for class' B timer to end.
// 5. Modulate gating time to prioritize a specific class
// NOTE: do not forget to regen the data by calling fillStack()

// EXAMPLE OF DATA TX
// error_handle = send_data(socket_handle, data_tx, PORT); 
```

In this project three implementation files are provided with their respective libraries. The idea is for you to implement the talker to send information to the receiver. Both must be executed within the same machine and only PORT will change per student. This will allow multiple students to work in the same machine without seeing conflicting traffic. Once CQF implemented in *talker_example.c*, head over to *listener_example.c* and validate the code you made in *talker_example.c*. The other files are to be used for documentation only.

```
├── bin --> Where executable files will be generated
├── doc --> You can find the presentation from class in here
│   ├── CQF_bd.png
│   └── Critical Embedded Systems.pdf
├── lib --> Libraries that enable the framework are found in here
│   ├── posix_sockets.c
│   └── posix_sockets.h
├── LICENSE
├── listener_example.c --> This code implements the receiver (TODO)
├── Makefile --> This file simplifies code compilation
├── obtain_time_example.c --> This code exemplifies how to obtain time data
├── README.md --> The file you are reading
└── talker_example.c --> This code implements the talker (TODO)
```

## Code compilation and execution
### Navigate directories
```bash
$ # To move within the repository
$ cd dir/
$ cat file # to see the contents of file
$ nano file # to edit a file (ctrl+o to save | ctrl+x to exit)
```

### Code compilation
```bash
$ # To compile code
$ make talker_example # To compile the talker
$ make listener_example # To compile the listener
$ make obtain_time_example # To compile the time retreival example
$ # every compilation will invoke gcc setting all warning flags as well as
$ # as well as debug ones
```

### Binary execution
```bash
$ # To execute code
$ bin/talker_example # To execute the talker
$ bin/listener_example # To execute the listener
$ bin/obtain_time_example # To execute time retreival example
```