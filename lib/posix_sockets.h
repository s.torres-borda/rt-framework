/**
 * @file posix_sockets.h
 * @author Santiago Torres Borda (santiago.torres-borda@isae-supaero.fr)
 * @brief This is the instatiation of posix_sockets library. It sets a way of creating a socket and using it for sending or receiving purposes.
 * @version 1.0
 * @date 2022-10-21
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef _POSIX_SOCKETS_H
#define _POSIX_SOCKETS_H 1

#define FAILURE_BIND 0
#define FAILURE_SOCKET 1
#define FAILURE_NON_BLOCKING 2
#define FAILURE_SEND 3

#define NON_BLOCKING 1

#define MAXLINE 1024 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <errno.h>

int init_socket();

int bind_socket(int socket_handle, unsigned short port);

struct sockaddr_in prepare_address(unsigned short port);

int send_data(int socket_handle, char *data, unsigned short port);

int recv_data(int socket_handle, char *buffer, unsigned short port);

#endif