/**
 * @file posix_sockets.c
 * @author Santiago Torres Borda (santiago.torres-borda@isae-supaero.fr)
 * @brief This is the implementation of posix_sockets library. It sets a way of creating a socket and using it for sending or receiving purposes.
 * @version 1.0
 * @date 2022-10-21
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "posix_sockets.h"

/**
 * @brief Initialize a socket
 *
 * This function creates a POSIX socket that is meant to send or receive datagrams through the network
 * 
 * @return Socket handle that is meant for identifying the created socket
 */
int init_socket(){
        int handle; // Verify socket status, will be used as boolean

        // Create a socket instance
        handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (handle <= 0){
                printf("failed to create socket\n");
                return FAILURE_SOCKET;
        }
        if (setsockopt(handle, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) < 0){
                printf("setsockopt(SO_REUSEADDR) failed");
                return FAILURE_SOCKET;
        }
        return handle;
}

/**
 * @brief Binds socket to a specific port
 *
 * Binds socket to listen on a specific port
 * 
 * @param socket_handle The handle computed through the output of init_socket()
 * @param port The port the socket will listen on
 * @return socket_handle The updated handle
 */
int bind_socket(int socket_handle, unsigned short port){
        struct sockaddr_in address;

        address = prepare_address(port);
        /* Socket bind & failure verification */
        if (bind(socket_handle, (const struct sockaddr*) &address, sizeof(struct sockaddr_in)) < 0){
                printf("failed to bind socket (%s)\n", strerror(errno)); // Cannot assign requested address
                return FAILURE_BIND;
        }
        
        return socket_handle;
}

/**
 * @brief prepare address
 *
 * Prepare data that will be used to route the information through the network
 * 
 * @param port The port that will ID information
 *
 * @return struct sockaddr_in The struct that contains packet ID information
 */
struct sockaddr_in prepare_address(unsigned short port){
        struct sockaddr_in address;

        memset((char *) &address, 0, sizeof(address));
        //strcpy(ip_address, inet_address);

        address.sin_family = AF_INET;
        address.sin_addr.s_addr = INADDR_ANY; // Set the source IP address
        address.sin_port = htons(port); // Specify the port which will tag the datagram

        return address;
}


/**
 * @brief Send data.
 *
 * This function sends a specified string through a UDP datagram using POSIX Sockets
 * 
 * @param socket_handle The handle computed through the output of init_socket()
 * @param data The data that is going to be sent through the datagram
 * @param port A specific port used for data handlig at reception
 * 
 * @return Error, if any, according to the situation
 */
int send_data(int socket_handle, char *data, unsigned short port){
        struct sockaddr_in address;

        address = prepare_address(port);

        // Check if interface is in a usable state
        if (fcntl(socket_handle, F_SETFL, O_NONBLOCK, NON_BLOCKING) == -1){
                printf("failed to set non-blocking\n");
                return FAILURE_NON_BLOCKING;
        }
        // Send data
        int sent_bytes = sendto(socket_handle, data, strlen(data), 0, (const struct sockaddr*) &address, sizeof(struct sockaddr_in));

        // Check if data was sent
        if (sent_bytes != strlen(data)){
                printf("failed to send packet\n");
                return FAILURE_SEND;
        } 
}

/**
 * @brief receive data
 * 
 * The function waits to receive data on the specified port.
 * 
 * @param socket_handle The handle computed through the output of init_socket()
 * @param buffer Where received data will be stored
 * @param port A specific port used for data handlig at reception
 * 
 * @return received_bytes_num The number of bytes received 
 */
int recv_data(int socket_handle, char *buffer, unsigned short port){
        /* Prepare data arrival */
        int len, received_bytes_num; 
        struct sockaddr_in address;

        address = prepare_address(port);
    
        len = sizeof(address);  //len is value/result 

        /* Wait for a datagram to arrive */
        received_bytes_num = recvfrom(socket_handle, (char *)buffer, MAXLINE, MSG_WAITALL, ( struct sockaddr *) &address, &len); 

        return received_bytes_num;
}