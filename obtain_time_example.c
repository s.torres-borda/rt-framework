/**
 * @file obtain_time_example.c
 * @author Santiago Torres Borda (santiago.torres-borda@isae-supaero.fr)
 * @brief This codes shows a quick example on how to obtain time from the OS with a nanosecond precision
 * @version 1.0
 * @date 2022-10-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

long long int epoch_s2ns(struct timespec time){
    return (time.tv_sec*(1000000000)) + time.tv_nsec;
}

int main() {
    /* Obtain system time */
    struct timespec time_now;
    clock_gettime(CLOCK_REALTIME, &time_now);

    /* Print epoch time */
    printf( "Time since 1/1/1970 (epoch): %ld.%09ld s\n", time_now.tv_sec, time_now.tv_nsec);
    printf( "Time since 1/1/1970 (epoch): %lld ns\n", epoch_s2ns(time_now));
    return EXIT_SUCCESS;
}